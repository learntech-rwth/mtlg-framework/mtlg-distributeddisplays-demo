/**
 * @Date:   2017-11-13T13:57:54+01:00
 * @Last modified time: 2017-11-13T17:27:14+01:00
 */



var MTLG=(function(m){
  var deviceOptions = {
    "zoll":27, //screen size in inches
    "mockLogin":true, //set to true to skip login procedure
    "mockNumber":1, //number of generated fake logins if mockLogin is true
    "loginMode":6, //Determines the login procedure for userLogin
    "language":"en", //Highest priority language setting, device dependant
  }

  m.loadOptions(deviceOptions);
  return m;
})(MTLG);
