/**
 * @Date:   2017-11-13T16:26:44+01:00
 * @Last modified time: 2017-11-13T16:38:13+01:00
 */
 /**
  * Creates a new room after asking for its name
  */
 var addRoom = function () {
     swal({
         type: 'question',
         title: locText("field1_addRoomTitle"),
         text: locText("field1_addRoomQuestion"),
         input: 'text',
         inputPlaceholder: locText("field1_addRoomPlaceholder"),
         allowEnterKey: true,
         focusConfirm: false,
         showCancelButton: true,
         inputValidator: function (value) {
             return new Promise(function (resolve, reject) {
                 if (value) {
                     resolve();
                 } else {
                     reject(locText("field1_emptyInput"));
                 }
             });
         }
     }).then(function (name) {
         MTLG.distributedDisplays.rooms.createRoom(name, function (result) {
             if (result && result.success) {
                 roomSelected(result.name);
             } else {
                 swal({
                     type: 'error',
                     title: locText("field1_addRoomErrorTitle"),
                     text: result.reason,
                     focusConfirm: true
                 });
             }
         });
     }, function (dismiss) {
         // User closed the dialog without adding a room
         // noop
     });
 };

 /**
  * Join an existing room
  * @param {object} event The click event
  * @param {string} name The name of the room to join
  */
 var joinRoomHandler = function (event, name) {
     MTLG.distributedDisplays.rooms.joinRoom(name, function (result) {
         if (result && result.success) {
             roomSelected(result.name);
         } else {
             swal({
                 type: 'error',
                 title: locText("field1_joinRoomErrorTitle"),
                 text: result.reason,
                 focusConfirm: true
             });
         }
     });
 };

 /**
  * Refreshes the list of existing rooms
  */
 var refreshRoomList = function () {
     MTLG.distributedDisplays.rooms.getAllRooms(function (result) {
         if (result && !(result.success === false)) {
             demo.field1.roomList.removeAllChildren();
             var x, y = 0;
             for (let roomName in result) {
                 var button = createButton(3 * demo.h, locText("field1_joinRoomButton"));
                 button.y = y;
                 button.on('click', joinRoomHandler, null, true, roomName);

                 if (!x) x = button.getBounds().width + demo.buttonMargin;

                 var text = new createjs.Text(roomName, demo.normalFont, demo.textColor);
                 text.textBaseline = 'middle'; // Align with button
                 text.x = x;
                 text.y = y + 1.5 * demo.h; // Align with button
                 demo.field1.roomList.addChild(button);
                 demo.field1.roomList.addChild(text);
                 y += 5 * demo.h;
             }
         }
     });
 };

 /**
  * Switches to the next field: Control selection
  * @param {string} roomName The name of the joined or created room
  */
 var roomSelected = function (roomName) {
     demo.roomName = roomName;
     demo.refreshFunction = null;
     demo.field1 = {};
     feld2_init();
 };



/**
 * Leaves the room and switches to the previous field: Room selection
 */
var leaveRoom = function () {
    MTLG.distributedDisplays.rooms.leaveRoom(demo.roomName, function (result) {
        if (result && result.success) {
            demo.refreshFunction = null;
            demo.refreshInterval = Infinity;
            demo.roomName = "";
            MTLG.distributedDisplays.SharedObject.getSharedObject(demo.sharedRoom.sharedId).delete();
            demo.sharedRoom = null;
            demo.field2 = {};
            feld1_init();
        } else {
            swal({
                type: 'error',
                title: locText("field1_leaveRoomErrorTitle"),
                text: result.reason,
                focusConfirm: true
            });
        }
    });
};
